use graphics::context::Context;
use graphics::Graphics;
use opengl_graphics::GlGraphics;
use piston::event_loop::{EventSettings, Events};
use piston::input::{Event, Input, Loop};
use piston::window::WindowSettings;
use sdl2_window::Sdl2Window;
use std::error::Error;

pub use image;
pub use graphics;
pub use opengl_graphics::OpenGL;
pub use piston::{event_loop, input, window};

pub trait Game<G>
where
    G: Graphics,
{
    type Error: Error + 'static;

    fn init(&mut self) -> Result<(), Self::Error>;

    fn input(&mut self, input: Input) -> Result<(), Self::Error>;

    fn update(&mut self, dt: f64) -> Result<(), Self::Error>;

    fn render(&mut self, graphics: &mut G, context: Context) -> Result<(), Self::Error>;
}

impl<'a, T, G> Game<G> for &'a mut T
where
    G: Graphics,
    T: Game<G>,
{
    type Error = T::Error;

    fn init(&mut self) -> Result<(), Self::Error> {
        Game::init(*self)
    }

    fn input(&mut self, input: Input) -> Result<(), Self::Error> {
        Game::input(*self, input)
    }

    fn update(&mut self, dt: f64) -> Result<(), Self::Error> {
        Game::update(*self, dt)
    }

    fn render(&mut self, graphics: &mut G, context: Context) -> Result<(), Self::Error> {
        Game::render(*self, graphics, context)
    }
}

pub fn run<G>(
    mut game: G,
    window_settings: WindowSettings,
    event_settings: EventSettings,
) -> Result<(), Box<dyn Error>>
where
    G: Game<GlGraphics>,
{
    let mut window: Sdl2Window = window_settings.build()?;
    let mut events = Events::new(event_settings);
    let mut graphics = GlGraphics::new(window_settings.get_maybe_opengl().unwrap_or(OpenGL::V3_2));

    game.init()?;

    while let Some(event) = events.next(&mut window) {
        match event {
            Event::Input(input) => {
                game.input(input)?;
            }
            Event::Loop(Loop::Update(args)) => {
                game.update(args.dt)?;
            }
            Event::Loop(Loop::Render(args)) => {
                graphics.draw(args.viewport(), |context, graphics| {
                    game.render(graphics, context)
                })?;
            }
            _ => {}
        }
    }

    Ok(())
}
