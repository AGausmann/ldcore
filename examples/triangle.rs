use ldcore::event_loop::EventSettings;
use ldcore::graphics::context::Context;
use ldcore::graphics::{clear, polygon, Graphics};
use ldcore::input::Input;
use ldcore::window::WindowSettings;
use ldcore::Game;
use std::convert::Infallible;

pub struct Triangle;

impl Game for Triangle {
    type Error = Infallible;

    fn init(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn input(&mut self, _input: Input) -> Result<(), Self::Error> {
        Ok(())
    }

    fn update(&mut self, _dt: f64) -> Result<(), Self::Error> {
        Ok(())
    }

    fn render<G>(&mut self, g: &mut G, context: Context) -> Result<(), Self::Error>
    where
        G: Graphics,
    {
        let viewport = context.viewport.unwrap();
        let width = viewport.rect[2] as f64;
        let height = viewport.rect[3] as f64;

        clear([0.0, 0.0, 0.0, 1.0], g);
        polygon(
            [1.0, 1.0, 1.0, 1.0],
            &[[0.0, height], [width / 2.0, 0.0], [width, height]],
            context.transform,
            g,
        );

        Ok(())
    }
}

fn main() {
    ldcore::run(
        Triangle,
        WindowSettings::new("LDCore Triangle Demo", (640, 480)),
        EventSettings::new(),
    )
    .unwrap();
}
